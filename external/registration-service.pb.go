// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0
// 	protoc        v3.12.4
// source: external/registration-service.proto

package external

import (
	context "context"
	proto "github.com/golang/protobuf/proto"
	common "gitlab.com/shadowy/sei/core/go-core-proto/common"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type IsExistRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *IsExistRequest) Reset() {
	*x = IsExistRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_external_registration_service_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *IsExistRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*IsExistRequest) ProtoMessage() {}

func (x *IsExistRequest) ProtoReflect() protoreflect.Message {
	mi := &file_external_registration_service_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use IsExistRequest.ProtoReflect.Descriptor instead.
func (*IsExistRequest) Descriptor() ([]byte, []int) {
	return file_external_registration_service_proto_rawDescGZIP(), []int{0}
}

func (x *IsExistRequest) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type IsExistResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Result bool `protobuf:"varint,1,opt,name=result,proto3" json:"result,omitempty"`
}

func (x *IsExistResponse) Reset() {
	*x = IsExistResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_external_registration_service_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *IsExistResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*IsExistResponse) ProtoMessage() {}

func (x *IsExistResponse) ProtoReflect() protoreflect.Message {
	mi := &file_external_registration_service_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use IsExistResponse.ProtoReflect.Descriptor instead.
func (*IsExistResponse) Descriptor() ([]byte, []int) {
	return file_external_registration_service_proto_rawDescGZIP(), []int{1}
}

func (x *IsExistResponse) GetResult() bool {
	if x != nil {
		return x.Result
	}
	return false
}

type AddRegistrationRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id   string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name string `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
}

func (x *AddRegistrationRequest) Reset() {
	*x = AddRegistrationRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_external_registration_service_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *AddRegistrationRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*AddRegistrationRequest) ProtoMessage() {}

func (x *AddRegistrationRequest) ProtoReflect() protoreflect.Message {
	mi := &file_external_registration_service_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use AddRegistrationRequest.ProtoReflect.Descriptor instead.
func (*AddRegistrationRequest) Descriptor() ([]byte, []int) {
	return file_external_registration_service_proto_rawDescGZIP(), []int{2}
}

func (x *AddRegistrationRequest) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *AddRegistrationRequest) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

type AddRegistrationResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *AddRegistrationResponse) Reset() {
	*x = AddRegistrationResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_external_registration_service_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *AddRegistrationResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*AddRegistrationResponse) ProtoMessage() {}

func (x *AddRegistrationResponse) ProtoReflect() protoreflect.Message {
	mi := &file_external_registration_service_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use AddRegistrationResponse.ProtoReflect.Descriptor instead.
func (*AddRegistrationResponse) Descriptor() ([]byte, []int) {
	return file_external_registration_service_proto_rawDescGZIP(), []int{3}
}

type RemoveRegistrationRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *RemoveRegistrationRequest) Reset() {
	*x = RemoveRegistrationRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_external_registration_service_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RemoveRegistrationRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RemoveRegistrationRequest) ProtoMessage() {}

func (x *RemoveRegistrationRequest) ProtoReflect() protoreflect.Message {
	mi := &file_external_registration_service_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RemoveRegistrationRequest.ProtoReflect.Descriptor instead.
func (*RemoveRegistrationRequest) Descriptor() ([]byte, []int) {
	return file_external_registration_service_proto_rawDescGZIP(), []int{4}
}

func (x *RemoveRegistrationRequest) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type RemoveRegistrationResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *RemoveRegistrationResponse) Reset() {
	*x = RemoveRegistrationResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_external_registration_service_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RemoveRegistrationResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RemoveRegistrationResponse) ProtoMessage() {}

func (x *RemoveRegistrationResponse) ProtoReflect() protoreflect.Message {
	mi := &file_external_registration_service_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RemoveRegistrationResponse.ProtoReflect.Descriptor instead.
func (*RemoveRegistrationResponse) Descriptor() ([]byte, []int) {
	return file_external_registration_service_proto_rawDescGZIP(), []int{5}
}

var File_external_registration_service_proto protoreflect.FileDescriptor

var file_external_registration_service_proto_rawDesc = []byte{
	0x0a, 0x23, 0x65, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x2f, 0x72, 0x65, 0x67, 0x69, 0x73,
	0x74, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x2d, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x08, 0x65, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x1a,
	0x14, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2f, 0x76, 0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x20, 0x0a, 0x0e, 0x49, 0x73, 0x45, 0x78, 0x69, 0x73, 0x74,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x22, 0x29, 0x0a, 0x0f, 0x49, 0x73, 0x45, 0x78, 0x69,
	0x73, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x16, 0x0a, 0x06, 0x72, 0x65,
	0x73, 0x75, 0x6c, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x08, 0x52, 0x06, 0x72, 0x65, 0x73, 0x75,
	0x6c, 0x74, 0x22, 0x3c, 0x0a, 0x16, 0x41, 0x64, 0x64, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72,
	0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a, 0x02,
	0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x12, 0x0a, 0x04,
	0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65,
	0x22, 0x19, 0x0a, 0x17, 0x41, 0x64, 0x64, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x61, 0x74,
	0x69, 0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x2b, 0x0a, 0x19, 0x52,
	0x65, 0x6d, 0x6f, 0x76, 0x65, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x61, 0x74, 0x69, 0x6f,
	0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x22, 0x1c, 0x0a, 0x1a, 0x52, 0x65, 0x6d, 0x6f,
	0x76, 0x65, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x32, 0xb2, 0x02, 0x0a, 0x13, 0x52, 0x65, 0x67, 0x69, 0x73,
	0x74, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x3a,
	0x0a, 0x07, 0x56, 0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e, 0x12, 0x16, 0x2e, 0x63, 0x6f, 0x6d, 0x6d,
	0x6f, 0x6e, 0x2e, 0x56, 0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x17, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x56, 0x65, 0x72, 0x73, 0x69,
	0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x3e, 0x0a, 0x07, 0x49, 0x73,
	0x45, 0x78, 0x69, 0x73, 0x74, 0x12, 0x18, 0x2e, 0x65, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c,
	0x2e, 0x49, 0x73, 0x45, 0x78, 0x69, 0x73, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a,
	0x19, 0x2e, 0x65, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x2e, 0x49, 0x73, 0x45, 0x78, 0x69,
	0x73, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x4a, 0x0a, 0x03, 0x41, 0x64,
	0x64, 0x12, 0x20, 0x2e, 0x65, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x2e, 0x41, 0x64, 0x64,
	0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x1a, 0x21, 0x2e, 0x65, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x2e, 0x41,
	0x64, 0x64, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x53, 0x0a, 0x06, 0x52, 0x65, 0x6d, 0x6f, 0x76, 0x65,
	0x12, 0x23, 0x2e, 0x65, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x2e, 0x52, 0x65, 0x6d, 0x6f,
	0x76, 0x65, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x24, 0x2e, 0x65, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c,
	0x2e, 0x52, 0x65, 0x6d, 0x6f, 0x76, 0x65, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x72, 0x61, 0x74,
	0x69, 0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x42, 0x75, 0x0a, 0x0c, 0x63,
	0x6f, 0x6d, 0x2e, 0x65, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x42, 0x13, 0x52, 0x65, 0x67,
	0x69, 0x73, 0x74, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x50, 0x01, 0x5a, 0x32, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x73,
	0x68, 0x61, 0x64, 0x6f, 0x77, 0x79, 0x2f, 0x73, 0x65, 0x69, 0x2f, 0x63, 0x6f, 0x72, 0x65, 0x2f,
	0x67, 0x6f, 0x2d, 0x63, 0x6f, 0x72, 0x65, 0x2d, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x65, 0x78,
	0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0xa2, 0x02, 0x03, 0x4d, 0x58, 0x58, 0xaa, 0x02, 0x08, 0x45,
	0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0xca, 0x02, 0x08, 0x45, 0x78, 0x74, 0x65, 0x72, 0x6e,
	0x61, 0x6c, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_external_registration_service_proto_rawDescOnce sync.Once
	file_external_registration_service_proto_rawDescData = file_external_registration_service_proto_rawDesc
)

func file_external_registration_service_proto_rawDescGZIP() []byte {
	file_external_registration_service_proto_rawDescOnce.Do(func() {
		file_external_registration_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_external_registration_service_proto_rawDescData)
	})
	return file_external_registration_service_proto_rawDescData
}

var file_external_registration_service_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_external_registration_service_proto_goTypes = []interface{}{
	(*IsExistRequest)(nil),             // 0: external.IsExistRequest
	(*IsExistResponse)(nil),            // 1: external.IsExistResponse
	(*AddRegistrationRequest)(nil),     // 2: external.AddRegistrationRequest
	(*AddRegistrationResponse)(nil),    // 3: external.AddRegistrationResponse
	(*RemoveRegistrationRequest)(nil),  // 4: external.RemoveRegistrationRequest
	(*RemoveRegistrationResponse)(nil), // 5: external.RemoveRegistrationResponse
	(*common.VersionRequest)(nil),      // 6: common.VersionRequest
	(*common.VersionResponse)(nil),     // 7: common.VersionResponse
}
var file_external_registration_service_proto_depIdxs = []int32{
	6, // 0: external.RegistrationService.Version:input_type -> common.VersionRequest
	0, // 1: external.RegistrationService.IsExist:input_type -> external.IsExistRequest
	2, // 2: external.RegistrationService.Add:input_type -> external.AddRegistrationRequest
	4, // 3: external.RegistrationService.Remove:input_type -> external.RemoveRegistrationRequest
	7, // 4: external.RegistrationService.Version:output_type -> common.VersionResponse
	1, // 5: external.RegistrationService.IsExist:output_type -> external.IsExistResponse
	3, // 6: external.RegistrationService.Add:output_type -> external.AddRegistrationResponse
	5, // 7: external.RegistrationService.Remove:output_type -> external.RemoveRegistrationResponse
	4, // [4:8] is the sub-list for method output_type
	0, // [0:4] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_external_registration_service_proto_init() }
func file_external_registration_service_proto_init() {
	if File_external_registration_service_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_external_registration_service_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*IsExistRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_external_registration_service_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*IsExistResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_external_registration_service_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*AddRegistrationRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_external_registration_service_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*AddRegistrationResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_external_registration_service_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RemoveRegistrationRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_external_registration_service_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RemoveRegistrationResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_external_registration_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_external_registration_service_proto_goTypes,
		DependencyIndexes: file_external_registration_service_proto_depIdxs,
		MessageInfos:      file_external_registration_service_proto_msgTypes,
	}.Build()
	File_external_registration_service_proto = out.File
	file_external_registration_service_proto_rawDesc = nil
	file_external_registration_service_proto_goTypes = nil
	file_external_registration_service_proto_depIdxs = nil
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// RegistrationServiceClient is the client API for RegistrationService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type RegistrationServiceClient interface {
	Version(ctx context.Context, in *common.VersionRequest, opts ...grpc.CallOption) (*common.VersionResponse, error)
	IsExist(ctx context.Context, in *IsExistRequest, opts ...grpc.CallOption) (*IsExistResponse, error)
	Add(ctx context.Context, in *AddRegistrationRequest, opts ...grpc.CallOption) (*AddRegistrationResponse, error)
	Remove(ctx context.Context, in *RemoveRegistrationRequest, opts ...grpc.CallOption) (*RemoveRegistrationResponse, error)
}

type registrationServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewRegistrationServiceClient(cc grpc.ClientConnInterface) RegistrationServiceClient {
	return &registrationServiceClient{cc}
}

func (c *registrationServiceClient) Version(ctx context.Context, in *common.VersionRequest, opts ...grpc.CallOption) (*common.VersionResponse, error) {
	out := new(common.VersionResponse)
	err := c.cc.Invoke(ctx, "/external.RegistrationService/Version", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *registrationServiceClient) IsExist(ctx context.Context, in *IsExistRequest, opts ...grpc.CallOption) (*IsExistResponse, error) {
	out := new(IsExistResponse)
	err := c.cc.Invoke(ctx, "/external.RegistrationService/IsExist", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *registrationServiceClient) Add(ctx context.Context, in *AddRegistrationRequest, opts ...grpc.CallOption) (*AddRegistrationResponse, error) {
	out := new(AddRegistrationResponse)
	err := c.cc.Invoke(ctx, "/external.RegistrationService/Add", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *registrationServiceClient) Remove(ctx context.Context, in *RemoveRegistrationRequest, opts ...grpc.CallOption) (*RemoveRegistrationResponse, error) {
	out := new(RemoveRegistrationResponse)
	err := c.cc.Invoke(ctx, "/external.RegistrationService/Remove", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// RegistrationServiceServer is the server API for RegistrationService service.
type RegistrationServiceServer interface {
	Version(context.Context, *common.VersionRequest) (*common.VersionResponse, error)
	IsExist(context.Context, *IsExistRequest) (*IsExistResponse, error)
	Add(context.Context, *AddRegistrationRequest) (*AddRegistrationResponse, error)
	Remove(context.Context, *RemoveRegistrationRequest) (*RemoveRegistrationResponse, error)
}

// UnimplementedRegistrationServiceServer can be embedded to have forward compatible implementations.
type UnimplementedRegistrationServiceServer struct {
}

func (*UnimplementedRegistrationServiceServer) Version(context.Context, *common.VersionRequest) (*common.VersionResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Version not implemented")
}
func (*UnimplementedRegistrationServiceServer) IsExist(context.Context, *IsExistRequest) (*IsExistResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method IsExist not implemented")
}
func (*UnimplementedRegistrationServiceServer) Add(context.Context, *AddRegistrationRequest) (*AddRegistrationResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Add not implemented")
}
func (*UnimplementedRegistrationServiceServer) Remove(context.Context, *RemoveRegistrationRequest) (*RemoveRegistrationResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Remove not implemented")
}

func RegisterRegistrationServiceServer(s *grpc.Server, srv RegistrationServiceServer) {
	s.RegisterService(&_RegistrationService_serviceDesc, srv)
}

func _RegistrationService_Version_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(common.VersionRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RegistrationServiceServer).Version(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/external.RegistrationService/Version",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RegistrationServiceServer).Version(ctx, req.(*common.VersionRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RegistrationService_IsExist_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(IsExistRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RegistrationServiceServer).IsExist(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/external.RegistrationService/IsExist",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RegistrationServiceServer).IsExist(ctx, req.(*IsExistRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RegistrationService_Add_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AddRegistrationRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RegistrationServiceServer).Add(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/external.RegistrationService/Add",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RegistrationServiceServer).Add(ctx, req.(*AddRegistrationRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RegistrationService_Remove_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RemoveRegistrationRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RegistrationServiceServer).Remove(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/external.RegistrationService/Remove",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RegistrationServiceServer).Remove(ctx, req.(*RemoveRegistrationRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _RegistrationService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "external.RegistrationService",
	HandlerType: (*RegistrationServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Version",
			Handler:    _RegistrationService_Version_Handler,
		},
		{
			MethodName: "IsExist",
			Handler:    _RegistrationService_IsExist_Handler,
		},
		{
			MethodName: "Add",
			Handler:    _RegistrationService_Add_Handler,
		},
		{
			MethodName: "Remove",
			Handler:    _RegistrationService_Remove_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "external/registration-service.proto",
}
