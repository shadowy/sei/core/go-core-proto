// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0
// 	protoc        v3.12.4
// source: security/model/system-row.proto

package model

import (
	proto "github.com/golang/protobuf/proto"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type SystemRow struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id            string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name          string `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Password      string `protobuf:"bytes,3,opt,name=password,proto3" json:"password,omitempty"`
	State         string `protobuf:"bytes,4,opt,name=state,proto3" json:"state,omitempty"`
	WorkWithEmail bool   `protobuf:"varint,5,opt,name=workWithEmail,proto3" json:"workWithEmail,omitempty"`
}

func (x *SystemRow) Reset() {
	*x = SystemRow{}
	if protoimpl.UnsafeEnabled {
		mi := &file_security_model_system_row_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SystemRow) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SystemRow) ProtoMessage() {}

func (x *SystemRow) ProtoReflect() protoreflect.Message {
	mi := &file_security_model_system_row_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SystemRow.ProtoReflect.Descriptor instead.
func (*SystemRow) Descriptor() ([]byte, []int) {
	return file_security_model_system_row_proto_rawDescGZIP(), []int{0}
}

func (x *SystemRow) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *SystemRow) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *SystemRow) GetPassword() string {
	if x != nil {
		return x.Password
	}
	return ""
}

func (x *SystemRow) GetState() string {
	if x != nil {
		return x.State
	}
	return ""
}

func (x *SystemRow) GetWorkWithEmail() bool {
	if x != nil {
		return x.WorkWithEmail
	}
	return false
}

var File_security_model_system_row_proto protoreflect.FileDescriptor

var file_security_model_system_row_proto_rawDesc = []byte{
	0x0a, 0x1f, 0x73, 0x65, 0x63, 0x75, 0x72, 0x69, 0x74, 0x79, 0x2f, 0x6d, 0x6f, 0x64, 0x65, 0x6c,
	0x2f, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x2d, 0x72, 0x6f, 0x77, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x12, 0x08, 0x73, 0x65, 0x63, 0x75, 0x72, 0x69, 0x74, 0x79, 0x22, 0x87, 0x01, 0x0a, 0x09,
	0x53, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x52, 0x6f, 0x77, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d,
	0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x1a, 0x0a,
	0x08, 0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x08, 0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x73, 0x74, 0x61,
	0x74, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x73, 0x74, 0x61, 0x74, 0x65, 0x12,
	0x24, 0x0a, 0x0d, 0x77, 0x6f, 0x72, 0x6b, 0x57, 0x69, 0x74, 0x68, 0x45, 0x6d, 0x61, 0x69, 0x6c,
	0x18, 0x05, 0x20, 0x01, 0x28, 0x08, 0x52, 0x0d, 0x77, 0x6f, 0x72, 0x6b, 0x57, 0x69, 0x74, 0x68,
	0x45, 0x6d, 0x61, 0x69, 0x6c, 0x42, 0x71, 0x0a, 0x0c, 0x63, 0x6f, 0x6d, 0x2e, 0x73, 0x65, 0x63,
	0x75, 0x72, 0x69, 0x74, 0x79, 0x42, 0x09, 0x53, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x52, 0x6f, 0x77,
	0x50, 0x01, 0x5a, 0x38, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x73,
	0x68, 0x61, 0x64, 0x6f, 0x77, 0x79, 0x2f, 0x73, 0x65, 0x69, 0x2f, 0x63, 0x6f, 0x72, 0x65, 0x2f,
	0x67, 0x6f, 0x2d, 0x63, 0x6f, 0x72, 0x65, 0x2d, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x73, 0x65,
	0x63, 0x75, 0x72, 0x69, 0x74, 0x79, 0x2f, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0xa2, 0x02, 0x03, 0x4d,
	0x58, 0x58, 0xaa, 0x02, 0x08, 0x53, 0x65, 0x63, 0x75, 0x72, 0x69, 0x74, 0x79, 0xca, 0x02, 0x08,
	0x53, 0x65, 0x63, 0x75, 0x72, 0x69, 0x74, 0x79, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_security_model_system_row_proto_rawDescOnce sync.Once
	file_security_model_system_row_proto_rawDescData = file_security_model_system_row_proto_rawDesc
)

func file_security_model_system_row_proto_rawDescGZIP() []byte {
	file_security_model_system_row_proto_rawDescOnce.Do(func() {
		file_security_model_system_row_proto_rawDescData = protoimpl.X.CompressGZIP(file_security_model_system_row_proto_rawDescData)
	})
	return file_security_model_system_row_proto_rawDescData
}

var file_security_model_system_row_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_security_model_system_row_proto_goTypes = []interface{}{
	(*SystemRow)(nil), // 0: security.SystemRow
}
var file_security_model_system_row_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_security_model_system_row_proto_init() }
func file_security_model_system_row_proto_init() {
	if File_security_model_system_row_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_security_model_system_row_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SystemRow); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_security_model_system_row_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_security_model_system_row_proto_goTypes,
		DependencyIndexes: file_security_model_system_row_proto_depIdxs,
		MessageInfos:      file_security_model_system_row_proto_msgTypes,
	}.Build()
	File_security_model_system_row_proto = out.File
	file_security_model_system_row_proto_rawDesc = nil
	file_security_model_system_row_proto_goTypes = nil
	file_security_model_system_row_proto_depIdxs = nil
}
